import * as plugins from './paypal.plugins';
import { PayPalAccount } from './paypal.classes.account';

export interface IPayPalOriginTransactionApiObject {
  transaction_info: {
    paypal_account_id: string;
    transaction_id: string;
    transaction_event_code: string;
    transaction_initiation_date: string;
    transaction_updated_date: string;
    transaction_amount: { currency_code: string; value: string };
    transaction_status: string;
    transaction_subject: string;
    ending_balance: { currency_code: string; value: string };
    available_balance: { currency_code: string; value: string };
    invoice_id: string;
    protection_eligibility: string;
  };
  cart_info: {
    item_details: {
      item_code: string;
      item_name: string;
      item_quantity: string;
      item_unit_price: {
        currency_code: string;
        value: string;
      };
      item_amount: {
        currency_code: string;
        value: string;
      };
      total_item_amount: {
        currency_code: string;
        value: string;
      };
      invoice_number: string;
    }[];
  };
  payer_info: {
    account_id: string;
    email_address: string;
    address_status: string;
    payer_status: string;
    payer_name: {
      alternate_full_name: string;
    };
    country_code: string;
  };
}

export interface IPayPalTransactionOptions {
  id: string;
  originApiObject: IPayPalOriginTransactionApiObject;
  amount: number;
  name: string;
  description: string;
  currency: 'USD' | 'EUR';
  timestampIso: string;
}

export class PayPalTransaction {
  public static async getTransactionFor30days(
    paypalInstanceArg: PayPalAccount,
    startPointMillis: number = Date.now() - plugins.smarttime.units.days(30)
  ) {
    const startDate = startPointMillis;
    const endDate = startDate + plugins.smarttime.units.days(30);
    const startDateIso = plugins.smarttime.ExtendedDate.fromMillis(startDate).toISOString();
    const endDateIso = plugins.smarttime.ExtendedDate.fromMillis(endDate).toISOString();
    console.log(`getting PayPal transactions from ${startDateIso} + ${endDateIso}`);
    const response = await paypalInstanceArg.request(
      'GET',
      `/v1/reporting/transactions?start_date=${startDateIso}&end_date=${endDateIso}&fields=all`,
      {}
    );

    const returnTransactions: PayPalTransaction[] = [];
    for (const transactionDetail of response.transaction_details) {
      const apiObject: IPayPalOriginTransactionApiObject = transactionDetail;
      const paypalTransaction = new PayPalTransaction({
        originApiObject: apiObject,
        id: apiObject.transaction_info.transaction_id,
        amount: parseFloat(apiObject.transaction_info.transaction_amount.value),
        currency: apiObject.transaction_info.transaction_amount.currency_code as 'EUR' | 'USD',
        timestampIso: apiObject.transaction_info.transaction_initiation_date,
        name: `${apiObject.payer_info.payer_name.alternate_full_name} (${apiObject.payer_info.email_address})`,
        description: `${apiObject.cart_info?.item_details?.length} items: ${apiObject.cart_info?.item_details?.map(itemArg => {
          return `${itemArg.item_name}`;
        }).reduce((accumulatorArg, currentValue) => {
          let returnString = '';
          if (accumulatorArg) {
            returnString = accumulatorArg + ', ' + currentValue;
          } else {
            returnString = currentValue;
          }
          return returnString;
        })}`,
      });
      returnTransactions.push(paypalTransaction);
    }
    return returnTransactions;
  }

  public data: IPayPalTransactionOptions;

  constructor(dataArg: IPayPalTransactionOptions) {
    this.data = dataArg;
  }
}
